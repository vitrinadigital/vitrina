import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vitrina/pages/forget_password.dart';
import 'package:vitrina/pages/login.dart';
import 'package:vitrina/pages/restore_password.dart';
import 'package:vitrina/pages/sign_up.dart';

class TextFieldWidget extends StatelessWidget {
  ///
  /// El widget [TextFieldWidget] son los campos de texto de:
  /// el login [LoginPage], el registro [SignUpPage], así como en las páginas
  /// para restaurar las contraseñas:
  /// [RestorePasswordPage], y [ForgetPasswordPage].
  ///
  /// Parámetros:
  /// *Con [hintText] se puede cambiar la descripcion que aparece dentro
  /// del campo antes de ingresar el texto y la etiqueta que se encuentra
  /// antes del campo de texto.
  ///
  /// * [withLabel] corresponde al controlador de la etiqueta que se encuentra
  /// antes del campo de texto.
  /// Si [withLabel] es True, la etiqueta va a ser visible y si es False,
  /// no se va a mostrar ese texto.
  ///
  /// * [isPassword] si este parámetro es True, el contenido del campo de texto
  /// se ocultará con *****, por defecto es False, ya que la mayoria
  /// de los campos no son de contraseñas.
  ///
  /// * [isEmail] si este parámetro es True, el teclado se mostrará con
  /// el @ 'arroba' para facilitar la escritura de un email válido. Por defecto
  /// es false.
  ///
  /// * Si está True [withSuffix], entonces se mostrará al final del campo de
  /// texto un Icono con el cual se puede hacer visible el texto ingresado.
  /// (Ideal para contraseñas).
  ///
  /// * [textEditingController] es el controlador del campo, es útil para
  /// obtener el valor ingresado en el campo.
  final String hintText;
  final bool withLabel;
  final bool isPassword;
  final bool isEmail;
  final bool withSuffix;
  final TextEditingController textEditingController;

  const TextFieldWidget({
    Key key,
    this.hintText = '',
    this.withLabel = true,
    this.isPassword = false,
    this.withSuffix = false,
    this.isEmail = false,
    this.textEditingController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Container(
        height: (withLabel) ? 80 : 50,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            if (withLabel)
              Text(
                hintText,
                textAlign: TextAlign.start,
                style: TextStyle(color: Colors.white),
              ),
            Container(
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                color: Colors.white,
              ),
              child: TextField(
                controller: textEditingController,
                decoration: InputDecoration.collapsed(
                  hintText: hintText,
                  //suffixIcon:(withSufix)?Icon(Icons.remove_red_eye):Icon
                ),
                obscureText: isPassword,
                keyboardType: (isPassword)
                    ? TextInputType.visiblePassword
                    : (isEmail)
                        ? TextInputType.emailAddress
                        : TextInputType.name,
              ),
            )
          ],
        ),
      ),
    );
  }
}
