import 'package:flutter/material.dart';

/// [ButtonWidget] es el botón usado en los formularios.
class ButtonWidget extends StatelessWidget {
  /// [text] es el texto que aparece en el botón
  /// [onTap] es la función a ejecutar cuando se pulse el botón
  final String text;
  final VoidCallback onTap;

  const ButtonWidget({
    Key key,
    this.text,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      child: Text(text),
      onPressed: onTap,
      elevation: 1,
      disabledElevation: 0,
      highlightElevation: 0,
      color: Color.fromARGB(255, 221, 182, 148),
      textColor: Color.fromARGB(255, 45, 76, 94),
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
    );
  }
}
