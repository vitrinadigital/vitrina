import 'package:flutter/material.dart';
import 'package:vitrina/controller/auth.dart';

/// [SocialWidget] es un widget que presenta los botones para
/// iniciar-registrarse con Google y Facebook. Se lo utilza en la página de
/// registro y de login.
class SocialWidget extends StatelessWidget {
  const SocialWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      width: 120,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            onTap: authWithFacebook,
            child: Image.asset(
              "assets/logo_fb.png",
              width: 40,
              height: 40,
            ),
          ),
          InkWell(
            onTap: authWithGoogle,
            child: Image.asset(
              "assets/logo_google.png",
              width: 40,
              height: 40,
            ),
          ),
        ],
      ),
    );
  }
}
