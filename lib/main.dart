import 'package:flutter/material.dart';
import 'package:vitrina/pages/login.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Vitrina Digital',
        theme: ThemeData(
          primarySwatch: Colors.teal,
          //La densidad de pixeles se adapta al dispositivo
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),

        /// Pagina de inicio
        home: LoginPage() //MyHomePage(title: 'Flutter Demo Home Page'),
        );
  }
}
