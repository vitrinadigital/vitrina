import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_auth_ui/firebase_auth_ui.dart';
import 'package:flutter/material.dart';
import 'package:vitrina/controller/navigator.dart';
import 'package:vitrina/pages/login.dart';
import 'package:vitrina/widgets/button_widget.dart';

/// [HomePage] es la página que se mostrará cuando el usuario ingrese a su
/// cuenta.
class HomePage extends StatelessWidget {
  const HomePage({
    Key key,
    @required this.userGlobal,
  }) : super(key: key);

  //Modelo de datos del usuario
  final User userGlobal;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 45, 76, 94),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              'Hola ${userGlobal.displayName} ${userGlobal.email}',
              style: TextStyle(color: Colors.white, fontSize: 19),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ButtonWidget(
                  text: 'Cerrar Sesión',
                  onTap: () async {
                    // Cerrar sesión y redirigir a Login
                    await FirebaseAuthUi.instance().logout();

                    pushReplaceNav(context, LoginPage());
                  }),
            )
          ],
        ),
      ),
    );
  }
}
