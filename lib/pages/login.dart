import 'dart:ui';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:vitrina/controller/navigator.dart';
import 'package:vitrina/pages/forget_password.dart';
import 'package:vitrina/pages/home.dart';
import 'package:vitrina/pages/sign_up.dart';
import 'package:vitrina/widgets/button_widget.dart';
import 'package:vitrina/widgets/social_widget.dart';
import 'package:vitrina/widgets/textfield_widget.dart';

/// [LoginPage] es la página de inicio de la app.
class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  /// [authState] es el estado de autenticación del usuario:
  /// False cuando no ha iniciado sesión.
  /// True cuando si ha iniciado sesión.
  bool authState;

  /// [userGlobal] es el modelo del usuario devuelto por firebase
  User userGlobal;

  //Controladores de los campos de texto
  TextEditingController textEmail = TextEditingController();
  TextEditingController textPassword = TextEditingController();

  /// Llave global del [Scaffold]
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  /// [_showScaffold] es un método para mostrar mensajes de advertencia
  /// en el actual [Scaffold]
  void _showScaffold(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(message),
    ));
  }

  @override
  void initState() {
    _initAsync();

    super.initState();
  }

  /// [_initAsync] es un llamado asincrono para inicializar Firebase
  /// e indentificar si el usuario se encuentra con la sesión iniciada o no
  /// De ser el caso, se lo redirige a [HomePage], caso contrario se muestra
  /// el formulario de login.
  _initAsync() async {
    FirebaseApp app = await Firebase.initializeApp();
    assert(app != null);

    FirebaseAuth.instance.authStateChanges().listen((User user) {
      if (user == null) {
        setState(() {
          //Sesion no iniciada
          authState = false;
        });
      } else {
        setState(() {
          //Sesion iniciada
          authState = true;
        });

        //Redirigir a Home
        pushReplaceNav(
          context,
          HomePage(userGlobal: user),
        );
      }
    });
  }

  @override
  Widget build(BuildContext mContext) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Color.fromARGB(255, 45, 76, 94),
      body: (authState == null)
          ? Center(child: CircularProgressIndicator())
          : (authState)
              ? Container()
              : Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 27.0),
                  child: SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    padding: const EdgeInsets.only(top: 120.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Hero(
                            transitionOnUserGestures: true,
                            tag: 'logo',
                            child: Image.asset(
                              "assets/name_color.png",
                              width: 235.0,
                              height: 105,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 50.0),
                          child: Text(
                            'Consectetur adipiscing elit, sed doeiusmod tempor in cididunt.',
                            style: TextStyle(color: Colors.white, fontSize: 19),
                          ),
                        ),
                        TextFieldWidget(
                          textEditingController: textEmail,
                          isEmail: true,
                          hintText: 'Correo Electrónico',
                        ),
                        TextFieldWidget(
                          textEditingController: textPassword,
                          hintText: 'Contraseña',
                          isPassword: true,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 48.0),
                          child: InkWell(
                            onTap: () {
                              //Redirigir a pagina para restaurar contraseña
                              pushNav(context, ForgetPasswordPage());
                            },
                            child: Text(
                              '¿Olvidaste tu contraseña?',
                              textAlign: TextAlign.right,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16),
                            ),
                          ),
                        ),
                        Hero(
                          tag: 'button',
                          child: ButtonWidget(
                            text: 'INICIAR SESIÓN',
                            onTap: () {
                              //Ingresar con usuario y contraseña
                              //TODO: extrapolar a un método para traducir los
                              //mensajes de error al Español
                              FirebaseAuth.instance
                                  .signInWithEmailAndPassword(
                                      email: textEmail.text,
                                      password: textPassword.text)
                                  .catchError((error) {
                                _showScaffold('${error.message}');
                              });
                            },
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.symmetric(vertical: 32.0),
                          child: Center(child: SocialWidget()),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Aún no tienes cuenta.',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white, fontSize: 19),
                              ),
                              InkWell(
                                onTap: () {
                                  pushNav(context, SignUpPage());
                                },
                                child: Text(
                                  ' Regístrate.',
                                  style: TextStyle(
                                      color: Color.fromARGB(255, 221, 182, 148),
                                      fontSize: 19),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
    );
  }
}
