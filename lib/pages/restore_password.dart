import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vitrina/widgets/textfield_widget.dart';

/// [RestorePasswordPage] es la página donde el usuario tiene que ingresar su
/// nueva contraseña.
/// Actualmente no está en uso ya que firebase implementa su propio formulario.
class RestorePasswordPage extends StatefulWidget {
  @override
  _RestorePasswordPageState createState() => _RestorePasswordPageState();
}

class _RestorePasswordPageState extends State<RestorePasswordPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 45, 76, 94),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 27.0),
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          padding: const EdgeInsets.only(top: 120.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: Image.asset(
                  "assets/name_color.png",
                  width: 167.0,
                  height: 75,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 64.0),
                child: Text(
                  'Reestablecer contraseña',
                  style: TextStyle(color: Colors.white, fontSize: 19),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: TextFieldWidget(
                  hintText: 'Contraseña',
                  withLabel: false,
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: TextFieldWidget(
                  hintText: 'Repetir Contraseña',
                  withLabel: false,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
