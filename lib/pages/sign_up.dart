import 'dart:ui';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vitrina/controller/navigator.dart';
import 'package:vitrina/widgets/button_widget.dart';
import 'package:vitrina/widgets/social_widget.dart';
import 'package:vitrina/widgets/textfield_widget.dart';

import 'home.dart';

/// [SignUpPage] es la página de registro
class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  /// [authState] es el estado de autenticación del usuario:
  /// False cuando no ha iniciado sesión.
  /// True cuando si ha iniciado sesión.
  bool authState;

  /// [userGlobal] es el modelo del usuario devuelto por firebase
  User userGlobal;

  //Controladores de los campos de texto
  TextEditingController textName = TextEditingController();
  TextEditingController textEmail = TextEditingController();
  TextEditingController textPassword = TextEditingController();
  TextEditingController textRepeatPassword = TextEditingController();

  /// Llave global del [Scaffold]
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  /// [_showScaffold] es un método para mostrar mensajes de advertencia
  /// en el actual [Scaffold]
  void _showScaffold(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(message),
    ));
  }

  @override
  void initState() {
    _initAsync();

    super.initState();
  }

  /// [_initAsync] es un llamado asincrono para indentificar si el usuario
  /// se encuentra con la sesión iniciada o no.
  /// De ser el caso, se lo redirige a [HomePage], caso contrario se muestra
  /// el formulario de login.
  _initAsync() async {
    FirebaseAuth.instance.authStateChanges().listen((User user) {
      if (user == null) {
        setState(() {
          authState = false;
        });
      } else {
        setState(() {
          authState = true;
        });

        //Redirigir a Home
        pushReplaceNav(context, HomePage(userGlobal: user));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Color.fromARGB(255, 45, 76, 94),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 27.0),
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          padding: const EdgeInsets.only(top: 120.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 45.0),
                  child: Hero(
                    tag: 'logo',
                    transitionOnUserGestures: true,
                    child: Image.asset(
                      "assets/name_color.png",
                      width: 167.0,
                      height: 75,
                    ),
                  ),
                ),
              ),
              TextFieldWidget(
                textEditingController: textName,
                hintText: 'Nombre de usuario',
                withLabel: false,
              ),
              TextFieldWidget(
                textEditingController: textEmail,
                isEmail: true,
                hintText: 'Correo Electrónico',
                withLabel: false,
              ),
              TextFieldWidget(
                textEditingController: textPassword,
                hintText: 'Contraseña',
                isPassword: true,
                withLabel: false,
              ),
              TextFieldWidget(
                textEditingController: textRepeatPassword,
                hintText: 'Repetir Contraseña',
                isPassword: true,
                withLabel: false,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20.0),
                child: Hero(
                  tag: 'button',
                  child: ButtonWidget(
                    text: 'REGISTRARSE',
                    onTap: _onSubmit,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20.0),
                child: Container(
                  height: 90,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'O puedes registrarte con',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white, fontSize: 19),
                      ),
                      SocialWidget(),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  //Método llamado cuando se envia el formulario de registro
  void _onSubmit() {
    //Comprobar si los campos estan llenos
    if (textName.text == '' ||
        textPassword.text == '' ||
        textEmail.text == '') {
      _showScaffold('Llena todos los campos.');
      return;
    }

    //Comprobar si las contraseñas coinciden
    if (textRepeatPassword.text != textPassword.text) {
      _showScaffold('Las contraseñas no coinciden.');
      return;
    }

    //Crear nuevo usuario
    FirebaseAuth.instance
        .createUserWithEmailAndPassword(
            email: textEmail.text, password: textPassword.text)
        .then((credentials) {
      credentials.user.updateProfile(displayName: textName.text);
    }).catchError((error) {
      _showScaffold('${error.message}');
    });
  }
}
