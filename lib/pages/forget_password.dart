import 'dart:ui';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vitrina/widgets/button_widget.dart';
import 'package:vitrina/widgets/textfield_widget.dart';

/// [ForgetPasswordPage] es la página que se muestra para restablecer la
/// contraseña
class ForgetPasswordPage extends StatefulWidget {
  @override
  _ForgetPasswordPageState createState() => _ForgetPasswordPageState();
}

class _ForgetPasswordPageState extends State<ForgetPasswordPage> {
  //Controlador del campo de texto de email
  TextEditingController textEmail = TextEditingController();

  /// Llave global del [Scaffold]
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  /// [_showScaffold] es un método para mostrar mensajes de advertencia
  /// en el actual [Scaffold]
  void _showScaffold(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(message),
    ));
  }

  /// [_forgetPassword] es el método que se ejecuta cuando el usuario pulsa el
  /// [ButtonWidget] de Enviar. Enviará un correo a la direccion que se ingrese
  /// en el campo de texto.
  void _forgetPassword() {
    FirebaseAuth.instance
        .sendPasswordResetEmail(email: textEmail.text)
        .then((value) => _showScaffold('Revisa tu bandeja de entrada'))
        .catchError((error) {
      _showScaffold('${error.message}');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Color.fromARGB(255, 45, 76, 94),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 27.0),
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          padding: const EdgeInsets.only(top: 120.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: Image.asset(
                  "assets/name_color.png",
                  width: 167.0,
                  height: 75,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 103, bottom: 32.0),
                child: Text(
                  'Ingresa un correo electrónico válido y a continuación te guiaremos en el proceso de reestablecer tu contraseña.',
                  style: TextStyle(color: Colors.white, fontSize: 19),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: TextFieldWidget(
                  textEditingController: textEmail,
                  isEmail: true,
                  hintText: 'Correo Electrónico',
                  withLabel: false,
                ),
              ),
              ButtonWidget(text: 'ENVIAR', onTap: _forgetPassword),
            ],
          ),
        ),
      ),
    );
  }
}
