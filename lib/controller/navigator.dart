import 'package:flutter/cupertino.dart';

// Duración de las transiciones entre páginas
const int _DURATION = 1000;

/// En este archivo se encuentran funciones relacionadas con la navegación
/// de la App

/// La función [_builder] es utilizada en [pushNav] y [pushReplaceNav]
/// En ella se configura las animaciones de las transiciones,
/// su tipo [FadeTransition] y  la duración.
PageRouteBuilder _builder(Widget widget) {
  return PageRouteBuilder(
    transitionDuration: Duration(milliseconds: _DURATION),
    pageBuilder: (BuildContext context, Animation<double> animation,
        Animation<double> secondaryAnimation) {
      return widget;
    },
    transitionsBuilder: (BuildContext context, Animation<double> animation,
        Animation<double> secondaryAnimation, Widget child) {
      return Align(
        child: FadeTransition(
          opacity: animation,
          child: child,
        ),
      );
    },
  );
}

//Abre una nueva página sin eliminar la actual
void pushNav(BuildContext context, Widget widget) {
  Navigator.of(context).push(_builder(widget));
}

//Abre una nueva página reemplazando a la actual
void pushReplaceNav(BuildContext context, Widget widget) {
  Navigator.of(context).pushReplacement(_builder(widget));
}

//Elimina la página actual.
void popNav(BuildContext context) {
  Navigator.pop(context);
}
