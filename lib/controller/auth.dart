import 'package:firebase_auth_ui/firebase_auth_ui.dart';
import 'package:firebase_auth_ui/providers.dart';
import 'package:vitrina/pages/login.dart';
import 'package:vitrina/pages/sign_up.dart';

/// [authWithGoogle] y [authWithFacebook] abrirán un modal, donde el usuario
/// deberá seleccionar su cuenta de Google o de Facebook, según sea el caso.
/// Para acceder al usuario en cualquier página de la app, se puede utilizar:
/// * FirebaseAuth.instance.currentUser

/// [authWithGoogle] Es una función que permite registrar un nuevo usuario
/// o iniciar sesión con la Sesión de una cuenta de Google, es utilizada
/// en [LoginPage] y [SignUpPage]
void authWithGoogle() {
  FirebaseAuthUi.instance()
      .launchAuth([AuthProvider.google()])
      .then((firebaseUser) =>
      print("Logged in user is ${firebaseUser.displayName}"))
      .catchError((error) => print("Error $error"));
}

/// [authWithFacebook] Es una función que permite registrar un nuevo usuario
/// o iniciar sesión con la Sesión de una cuenta de Google, es utilizada
/// en [LoginPage] y [SignUpPage]
void authWithFacebook() {
  FirebaseAuthUi.instance()
      .launchAuth([AuthProvider.facebook()])
      .then((firebaseUser) =>
      print("Logged in user is ${firebaseUser.displayName}"))
      .catchError((error) => print("Error $error"));
}